
public class Tree {
	public enum Op{
		AND, OR, NOT, VAR, IMP, KVANT;
		public String toString(){
			switch(this){
				case AND: return "&";
				case OR: return "v";
				case NOT: return "~";
				case VAR: return "VAR";
				case IMP: return "->";
				case KVANT: return "KVANT";
			}
			return "???";
		}
	}
	
	public enum Kvant{
		EXS, EVR, VAR;
		public String toString(){
			switch(this){
				case EXS: return "Exist";
				case EVR: return "For all";
				case VAR: return "VAR";
			}
			return "???";
		}
	}
	
	
	
	public Tree left;
	public Tree right;
	public int id;
	public Op op;
	public Kvant kvant;
	public boolean value;
	
	public Tree(Kvant k, int x, Tree y){
		op = op.VAR;
		kvant = k;
		right = null;
		left = y;
		id = x;
	}
	public Tree(Op o, Tree x){
		kvant = null;
		left=x; 
		right=null; 
		op=o;
	}
	public Tree(Op o, Tree x, Tree y){
		left=x;
		right=y;
		op=o;
		id=0; 
	}
	public Tree(int i){
		left=right=null;
		op=op.VAR;
		kvant = kvant.VAR;
		id=i;
	}
	
	public String toString(){
		switch(op){
			case AND:case OR:case IMP:
				return "(" + left.toString() + " " + op.toString() + " " + right.toString() + ")";
			case NOT:
				return op.toString() + left.toString();
			case VAR:
				switch(kvant){
				case EVR:case EXS:
					return kvant.toString() + " " + "A" + Integer.toString(id) + ": " + left.toString();
				case VAR: return "A" + Integer.toString(id);
			}
			default: return "???";	
		}
		
	}
}
