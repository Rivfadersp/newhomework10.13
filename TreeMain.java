import java.io.IOException;
import java.util.Scanner;

public class TreeMain {
	
	
	public static int maxelement(Tree t){                   //Max index
		if(t.right==null && t.left==null) return t.id;
		if(t.right==null) return maxelement(t.left);
		return Math.max(maxelement(t.right), maxelement(t.left));
	}
	
	
	public static boolean result(Tree t, boolean[] val){           //Value
		if (t.right==null && t.left==null){
			return val[t.id-1];
		}
		if (t.right!=null && t.left!=null){
			if(t.op.toString()=="&"){
				if(result(t.left, val)==true && result(t.right, val)==true) return true;
			}
			if(t.op.toString()=="v"){
				if(result(t.left, val)==true | result(t.right, val)==true) return true;
			}
			if(t.op.toString()=="->"){
				if(result(t.left, val)==true && result(t.right, val)==false) return false;
				else return true;
			}
		}
		if(t.right==null && t.kvant==null){
			if(result(t.left, val)==true) return false;
			else return true;
		}
		if(t.right==null){
			return result(t.left, val);
		}
		return false;
	}
	
	public static boolean resultkvant(Tree t, boolean[] val, boolean prep){             //Value with quantifiers
		boolean[] val2 = new boolean[val.length];
		if(t.kvant.toString()=="Exist"){
			val[t.id-1]=true;
			if(result(t, val)==prep) return true;
			val[t.id-1]=false;
			if(result(t, val)==prep) return true;
		}
		if(t.kvant.toString()=="For all"){
			val[t.id-1]=true;
			if(result(t,val)!=prep) return false;
			val[t.id-1]=false;
			if(result(t,val)!=prep) return false;
			return true;
		}
		return false;
	}
	
	
	static public boolean test(Tree t, boolean[] val, int count){                                  //SAT
		if(result(t, val)==true) return true;
		if(count<maxelement(t)){
			val[count] = true;
			if(test(t, val, count+1)==true) return true;
			val[count] = false;
			if(test(t, val, count+1)==true) return true;
		}
		return false;
	}
	
	static public boolean test1(Tree t, boolean[] val, int count){                                  //#SAT
		if(resultkvant(t, val, true)) return true;
		if(count<maxelement(t)){
		
				val[count] = true;
				if(test1(t, val, count+1)==true) return true;
				val[count] = false;
				if(test1(t, val, count+1)==true) return true;
		
		}
		return false;
	}

	/*static public void foundfun(boolean[] table, boolean[] val, int current){                 //������
		Tree.Op[] op = {Tree.Op.AND, Tree.Op.OR};
		val[0]=table[0];
		val[1]=table[1];
		Tree t = new Tree(op[current], new Tree(1), new Tree(2));
		if (result(t, val)==table[2]) System.out.println("���� "+ current);
		else foundfun(table, val, current+1);
	}*/
	
	public static void main(String[] args){
		
		
		//Tree t = new Tree(Tree.Op.NOT, new Tree(Tree.Op.AND, new Tree(1), new Tree(2)));
		
		Tree t = new Tree(Tree.Kvant.EVR, 1, new Tree(Tree.Op.AND, new Tree(Tree.Op.OR, new Tree(1), new Tree(2)), new Tree(Tree.Op.NOT, new Tree(1))));
		boolean g = false;
		String st = "";
		
		boolean[] val = new boolean[maxelement(t)];
		boolean[] table = {true, true, false};
		
					
		Scanner s = new Scanner(System.in);
		
	
		
		System.out.println(t.toString());
		System.out.println(test1(t, val, 0));
		
		
	}

}
